/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainteste;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author João Paulo
 */
public class NumerosTest {
    private Numeros n;
    
    public NumerosTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        n = new Numeros();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of numPar method, of class Numeros.
     */
    @Test
    public void testNumPar() {
       assertEquals(true,n.numPar(4)); 
    }

    /**
     * Test of quadrado method, of class Numeros.
     */
    @Test
    public void testQuadrado() {
        assertEquals(25,n.quadrado(5));
    }
    
}
